# FTR Challenge

## Instructions

```bash
git clone https://gitlab.com/jeremy.jackson/ftr.git # Get the project
npm install # Install dependencies
npm run build-test # Run the build and test scripts
npm run start # Run the application
```

Note: I didn't write good integration and regression tests, since that can be tough to
do for applications that depend on `readline`-like functionality.
I did write a [CI/CD pipeline](https://gitlab.com/jeremy.jackson/ftr/pipelines) though.

## The challenge

The challenges in Part 2 really inform a lot about the requirements about how
Part 1 needs to be written to make Part 2 as simple as possible, so I'm going
to address Part 2 before I start in on writing Part 1.

## Part 2

### Question 1

> You have a new requirement to implement for your application: its logic should stay
> exactly the same but it will need to have a different user interface (e.g. if you wrote a
> web app, a different UI may be a REPL).
>  
> Please describe how you would go about implementing this new UI in your application?
> Would you need to restructure your solution in any way?

Based on this requirement, something akin to
[Uncle Bob's Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
is probbaly the best way to structure this project.

![Uncle Bob's Clean Architecture](assets/UncleBob-CleanArchitecture.jpg)

By choosing this style of architecture I can write low-level classes and functions to
implement the UI-invariant business rules, data structures, and actions.
Once those are defined, then the UI I choose pretty much doesn't matter, and I should
be able to implement the solution for that UI with little-to-no modification of the
low-level implementation.

Another benefit of choosing this architecture is that becomes almost trivial to write
tests and mock classes and interfaces.

So, to support this sort of architecture, I've opted to split the code base between
`lib/`and `cli/`, where `lib/` contains the UI-invariant low-level implementation.
Were I doing this for real, I would probably publish the contents of `lib/` as it's
own NPM package and `cli/` as it's own pacakge that depends on the package created
out of `lib/`.

### Question 2

> You now need to make your application “production ready”, and deploy it so that it can
be used by customers.
>
> Please describe the steps you’d need to take for this to happen.

In this case, since I chose to make an interactive command-line application I would
prep this to be "production ready" by doing what I suggested in the last paragraph
of my answer to Part 1.

1. Break this into a UI-invariant package and a package that implements the UI as a
command-line application.
2. In the `package.json` for the cli application I would have to include a `bin` key
and accompanying value so that someone could do an `npm install -g ...` on that package
and thereby be able to have the package symlinked and available as if it were any other
installed application. I would also go through the steps of making sure that I have a
public way to share results of tests run by CI and add some badges to the repo reflecting
the build status and the current version. I'd also have to make sure that my license is
selected appropriately for both `lib/` and `cli/`, fully write out usage documentation
for both `lib/` and `cli/`, and include contribution guidelines if I intend to allow community
contributions.
3. Publish to NPM.

Were this a web-based UI, I would have to make sure that I'm running an
appropriate build method: 

- **Can this just be compiled to a static file artifact that gets
deployed to a well-known webserver?** `sftp` the artifact to the appropriate
server and directory near the end of the CICD pipeline, and the pipeline should be written to
ensure that the correct permissions are applied to the files. Might have to bounce
the webserver to clear old cached versions of the files if that is an anticipated problem, but
again, that should be in the pipeline.
- **Does this rely on a specific version of a some other project that also needs to be updated?**
We might have to do some coordination and notify customers of potential service disruptions
during our maintenance window if they have to be deployed simultaneously by different CICD
pipelines, unless we have some good zero-downtime HA practices in place.
If we are able to bundle the releases into the same CICD pipeline, then we should be able to
create a deployment artifact such as a container image or serverless-optimized artifact.
The CICD pipeline should build everything together into the single deployment artifact, which
gets loaded to some artifact repository, and then pulled from there into our staging system for
verification before it gets pulled into the production system, all via commands given by the CICD
pipeline, which may or may not require approvals from humans to progress to subsequent stages.

### Question 3

> What did you think about this coding test - is there anything you’d suggest in order to
> improve it?

Maybe allow bonus points for doing different things such as complete documentation, unit and
integration tests, CI pipelines, creation of deployment artifacts, etc.

Having the challenge be a little more oriented towards what you're hoping a successful candidate
will bring to the team would be pretty good as well. From this challenge the only thing I can
really tell is that you need someone that understands good design and can translate requirements
into architectural design decisions, which I would think would be a given for the level you're hiring for.
