import * as Inquirer from 'inquirer';

import Timer from './lib/timer';
import ListOfNumbers from './lib/list-of-numbers';

import startup from './cli/startup';
import nextNumberLoop from './cli/next-number-loop';

startup(Inquirer)
  .then((answers) => {
    const { outputInterval } = answers;
    const interval = Number(outputInterval) * 1000;

    const listOfNumbers = new ListOfNumbers();
    const timer = new Timer(() => {
      const lon = listOfNumbers.byDescendingValues();
      const barText = lon.reduce((acc, cv) => `${acc} ${cv[0]}:${cv[1]},`, '');
      console.log(barText.substring(0, barText.length - 1));
    }, interval);

    return nextNumberLoop(Inquirer, timer, listOfNumbers);
  })
  .then(({ timer, listOfNumbers }) => {
    // Cleanup and print exit messages.
    timer.halt();

    const lon = listOfNumbers.byDescendingValues();
    const barText = lon.reduce((acc, cv) => `${acc} ${cv[0]}:${cv[1]},`, '');
    console.log(barText.substring(0, barText.length - 1));
    console.log('Thanks for playing!');
  });
