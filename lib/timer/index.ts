export default class Timer {
  cb: () => void;
  millis: number;
  runningSince: number;
  t: any; // Intervals and Timeouts are different Types between Browser and Node.
  msRemaining: number = 0;

  constructor(callback: () => void, millis: number) {
    this.cb = callback;
    this.millis = millis;
    this.runningSince = Date.now();
    this.t = <any>setInterval(() => {
      this.cb();
      this.runningSince = Date.now();
    }, this.millis);
  }

  halt() {
    this.msRemaining = this.millis - (Date.now() - this.runningSince);
    clearInterval(this.t);
  }

  resume() {
    if (this.t._destroyed) {
      this.runningSince = Date.now();
      this.t = <any>setTimeout(() => {
        this.cb();
        this.runningSince = Date.now();
        this.t = <any>setInterval(this.cb, this.millis);
      }, this.msRemaining);
    }
  }
}
