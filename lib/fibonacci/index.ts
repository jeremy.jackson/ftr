/*
 * Should reimplement this to be a WASM that either uses the GNU MP
 * library or takes advantage of Binet's formula so that we can get
 * compiled language computation time benefits while still being
 * agnostic to Browser vs. Node.
 *
 * https://gmplib.org
 * https://en.wikipedia.org/wiki/Fibonacci_number#Binet's_formula
 */
export default class Fibonacci {
  readonly fibList1000 = [BigInt(1), BigInt(1)];

  constructor() {
    let lo = this.fibList1000[0];
    let hi = this.fibList1000[1];

    while (this.fibList1000.length < 1000) {
      let next = lo + hi;
      this.fibList1000.push(next);
      lo = hi;
      hi = next;
    }
  }

  public isFib1000(x: string) {
    return this.fibList1000.includes(BigInt(x));
  }
}
