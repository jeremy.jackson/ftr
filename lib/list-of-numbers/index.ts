export default class ListOfNumbers {
  list: { [index: string]: number };

  constructor() {
    this.list = {};
  }

  increment(value: string) {
    const val = Number(value);
    if (isNaN(val) || !Number.isInteger(val)) throw new Error('value must be an integer');

    if (this.list[value]) {
      this.list[value] += 1
    } else {
      this.list[value] = 1
    }
  }

  byDescendingValues() {
    return Object.entries(this.list).sort((a, b) => b[1] - a[1]);
  }
}
