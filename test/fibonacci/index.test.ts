import { expect } from 'chai';

import Fibonacci from '../../lib/fibonacci';

describe('isFib1000', () => {
  const fib = new Fibonacci();

  describe('should return appropriate values for low fibonacci numbers', () => {
    const fib1 = fib.isFib1000('1');
    const fib2 = fib.isFib1000('2');
    const fib3 = fib.isFib1000('3');
    const fib4 = fib.isFib1000('4');
    const fib5 = fib.isFib1000('5');
    it('should be truthy for 1', () => expect(fib1).to.be.true);
    it('should be truthy for 2', () => expect(fib2).to.be.true);
    it('should be truthy for 3', () => expect(fib3).to.be.true);
    it('should be falsey for 4', () => expect(fib4).to.be.false);
    it('should be truthy for 5', () => expect(fib5).to.be.true);
  });

  describe('should return appropriate values for high fibonacci numbers', () => {
    /*
     * magic Fibonacci numbers from
     * https://www.wolframalpha.com/widgets/view.jsp?id=37c192d1af619c2877c9891f396e344c
     */
    const num999 = '26863810024485359386146727202142923967616609318986952340123175997617981700247881689338369654483356564191827856161443356312976673642210350324634850410377680367334151172899169723197082763985615764450078474174626';
    const fib999 = fib.isFib1000(num999);
    it(`should be truthy for 999th fibonacci number`, () => expect(fib999).to.be.true);

    const num1000 = '43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228875';
    const fib1000 = fib.isFib1000(num1000);
    it(`should be truthy for 1000th fibonacci number`, () => expect(fib1000).to.be.true);

    const num1000plus1 = '43466557686937456435688527675040625802564660517371780402481729089536555417949051890403879840079255169295922593080322634775209689623239873322471161642996440906533187938298969649928516003704476137795166849228876';
    const fib1000plus1 = fib.isFib1000(num1000plus1);
    it(`should be falsey for (1 + {1000th fibonacci number}) ${fib1000plus1}`, () => expect(fib1000plus1).to.be.false);

    const num1001 = '70330367711422815821835254877183549770181269836358732742604905087154537118196933579742249494562611733487750449241765991088186363265450223647106012053374121273867339111198139373125598767690091902245245323403501';
    const fib1001 = fib.isFib1000(num1001);
    it(`should be falsey for 1001st fibonacci number`, () => expect(fib1001).to.be.false);
  });
});
