import { expect } from 'chai';
import Timer from '../../lib/timer';

describe('Timer', () => {
  describe('properties', () => {
    const myTimer = new Timer(() => {}, 100000);

    it('should have property #millis', () => expect(myTimer).to.have.property('millis'));
    it('should have property #runningSince', () => expect(myTimer).to.have.property('runningSince'));
    it('should have property #t(imer)', () => expect(myTimer).to.have.property('t'));
    it('should have property #msRemaining', () => expect(myTimer).to.have.property('msRemaining'));
    it('should have an active #timer', () => expect(myTimer.t._destroyed).to.be.false);
  });

  describe('#halt', () => {
    const myTimer = new Timer(() => {}, 100000);


    before((done) => {
      const now = Date.now();
      while (now === Date.now()) {
        (() => {})();
      }
      myTimer.halt();
      done();
    });

    describe('msRemaining', () => {
      it('should set msRemaining', () => expect(myTimer.msRemaining).to.be.below(myTimer.millis));
      it('should not be 0', () => expect(myTimer.msRemaining).to.be.above(0));
    });
    describe('destroyed', () => {
      it('should set timer to destroyed', () => expect(myTimer.t._destroyed).to.be.true);
    });
  });

  describe('#resume', () => {
    describe('if #halt() was not called first', () => {
      const myTimer = new Timer(() => {}, 10000);
      const old = { runningSince: myTimer.runningSince };

      before((done) => {
        let now = Date.now();
        while (now === Date.now()) {
          (() => {})();
        }

        myTimer.resume();

        now = Date.now();
        while (now === Date.now()) {
          (() => {})();
        }
        done();
      });

      describe('same runningSince', () => {
        it('should have the same #runningSince', () => {
          expect(myTimer.runningSince).to.equal(old.runningSince)
        });
      });
    });

    describe('if #halt() was called first', () => {
      const myTimer = new Timer(() => {}, 10000);
      const old = { runningSince: myTimer.runningSince };

      before((done) => {
        let now = Date.now();
        while (now === Date.now()) {
          (() => {})();
        };

        myTimer.halt();
        myTimer.resume();

        now = Date.now();
        while (now === Date.now()) {
          (() => {})();
        };
        done();
      });

      describe('different runningSince', () => {
        it('should not have the same #runningSince', () => {
          expect(myTimer.runningSince).to.not.equal(old.runningSince)
        });
      });
    });
  });
});
