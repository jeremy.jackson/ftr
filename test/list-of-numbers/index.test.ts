import { expect } from 'chai';
import ListOfNumbers from '../../lib/list-of-numbers';

describe('ListOfNumbers', () => {
  let listOfNumbers: ListOfNumbers;

  beforeEach(() => {
    listOfNumbers = new ListOfNumbers();
  });

  describe('#list', () => {
    it('should have a `list` property', () => {
      expect(listOfNumbers).to.have.property('list');
    });
  });

  describe('#increment()', () => {
    it('should reject any non-integer number argument', () => {
      expect(() => listOfNumbers.increment('1.2')).to.throw()
    });

    it('should update #list with new keys and values', () => {
      listOfNumbers.increment('1');
      listOfNumbers.increment('1');
      listOfNumbers.increment('2');
      expect(listOfNumbers.list).to.eql({ 1: 2, 2: 1 });
    });
  });

  describe('#byDescendingValues()', () => {
    it('should return the list in descending frequency order as an array of arrays', () => {
      listOfNumbers.increment('1');
      listOfNumbers.increment('2');
      listOfNumbers.increment('2');
      listOfNumbers.increment('2');
      listOfNumbers.increment('3');
      listOfNumbers.increment('3');
      const output = listOfNumbers.byDescendingValues();
      expect(output).to.eql([['2', 3], ['3', 2], ['1', 1]]);
    });
  });
});
