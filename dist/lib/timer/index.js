"use strict";
exports.__esModule = true;
var Timer = /** @class */ (function () {
    function Timer(callback, millis) {
        var _this = this;
        this.msRemaining = 0;
        this.cb = callback;
        this.millis = millis;
        this.runningSince = Date.now();
        this.t = setInterval(function () {
            _this.cb();
            _this.runningSince = Date.now();
        }, this.millis);
    }
    Timer.prototype.halt = function () {
        this.msRemaining = this.millis - (Date.now() - this.runningSince);
        clearInterval(this.t);
    };
    Timer.prototype.resume = function () {
        var _this = this;
        if (this.t._destroyed) {
            this.runningSince = Date.now();
            this.t = setTimeout(function () {
                _this.cb();
                _this.runningSince = Date.now();
                _this.t = setInterval(_this.cb, _this.millis);
            }, this.msRemaining);
        }
    };
    return Timer;
}());
exports["default"] = Timer;
