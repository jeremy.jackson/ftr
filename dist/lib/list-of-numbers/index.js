"use strict";
exports.__esModule = true;
var ListOfNumbers = /** @class */ (function () {
    function ListOfNumbers() {
        this.list = {};
    }
    ListOfNumbers.prototype.increment = function (value) {
        var val = Number(value);
        if (isNaN(val) || !Number.isInteger(val))
            throw new Error('value must be an integer');
        if (this.list[value]) {
            this.list[value] += 1;
        }
        else {
            this.list[value] = 1;
        }
    };
    ListOfNumbers.prototype.byDescendingValues = function () {
        return Object.entries(this.list).sort(function (a, b) { return b[1] - a[1]; });
    };
    return ListOfNumbers;
}());
exports["default"] = ListOfNumbers;
