"use strict";
exports.__esModule = true;
/*
 * Should reimplement this to be a WASM that either uses the GNU MP
 * library or takes advantage of Binet's formula so that we can get
 * compiled language computation time benefits while still being
 * agnostic to Browser vs. Node.
 *
 * https://gmplib.org
 * https://en.wikipedia.org/wiki/Fibonacci_number#Binet's_formula
 */
var Fibonacci = /** @class */ (function () {
    function Fibonacci() {
        this.fibList1000 = [BigInt(1), BigInt(1)];
        var lo = this.fibList1000[0];
        var hi = this.fibList1000[1];
        while (this.fibList1000.length < 1000) {
            var next = lo + hi;
            this.fibList1000.push(next);
            lo = hi;
            hi = next;
        }
    }
    Fibonacci.prototype.isFib1000 = function (x) {
        return this.fibList1000.includes(BigInt(x));
    };
    return Fibonacci;
}());
exports["default"] = Fibonacci;
