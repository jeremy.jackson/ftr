"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var Inquirer = __importStar(require("inquirer"));
var timer_1 = __importDefault(require("./lib/timer"));
var list_of_numbers_1 = __importDefault(require("./lib/list-of-numbers"));
var startup_1 = __importDefault(require("./cli/startup"));
var next_number_loop_1 = __importDefault(require("./cli/next-number-loop"));
startup_1["default"](Inquirer)
    .then(function (answers) {
    var outputInterval = answers.outputInterval;
    var interval = Number(outputInterval) * 1000;
    var listOfNumbers = new list_of_numbers_1["default"]();
    var timer = new timer_1["default"](function () {
        var lon = listOfNumbers.byDescendingValues();
        var barText = lon.reduce(function (acc, cv) { return acc + " " + cv[0] + ":" + cv[1] + ","; }, '');
        console.log(barText.substring(0, barText.length - 1));
    }, interval);
    return next_number_loop_1["default"](Inquirer, timer, listOfNumbers);
})
    .then(function (_a) {
    var timer = _a.timer, listOfNumbers = _a.listOfNumbers;
    // Cleanup and print exit messages.
    timer.halt();
    var lon = listOfNumbers.byDescendingValues();
    var barText = lon.reduce(function (acc, cv) { return acc + " " + cv[0] + ":" + cv[1] + ","; }, '');
    console.log(barText.substring(0, barText.length - 1));
    console.log('Thanks for playing!');
});
