"use strict";
exports.__esModule = true;
function startup(inquirer) {
    var question = [
        {
            type: 'number',
            name: 'outputInterval',
            message: 'Please input the number of time in seconds between emitting numbers and their frequency.',
            "default": 10,
            validate: function (x) { return Number(x) > 0; }
        }
    ];
    return inquirer.prompt(question);
}
exports["default"] = startup;
;
