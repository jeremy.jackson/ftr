"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var fibonacci_1 = __importDefault(require("../../lib/fibonacci"));
function nextNumberLoop(inquirer, timer, listOfNumbers) {
    return __awaiter(this, void 0, void 0, function () {
        var fib, validWordCommands, keepGoing, question, answer, nextNumber, nn;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    fib = new fibonacci_1["default"]();
                    validWordCommands = ['quit', 'halt', 'resume'];
                    keepGoing = true;
                    question = [
                        {
                            type: 'input',
                            name: 'nextNumber',
                            "default": '0',
                            message: 'Please enter the next number or one of these commands: halt, resume, or quit.',
                            validate: function (x) {
                                var logMessage = '';
                                var isBigInt = false;
                                var isValidWordCommand = false;
                                try {
                                    BigInt(x);
                                    isBigInt = true;
                                }
                                catch (err) {
                                    logMessage += err.message;
                                }
                                if (validWordCommands.includes(x)) {
                                    isValidWordCommand = true;
                                }
                                else {
                                    logMessage += 'Not a valid command word.';
                                }
                                if (!isBigInt && !isValidWordCommand)
                                    console.log(logMessage);
                                return isBigInt || isValidWordCommand;
                            }
                        },
                    ];
                    _a.label = 1;
                case 1:
                    if (!keepGoing) return [3 /*break*/, 3];
                    return [4 /*yield*/, inquirer.prompt(question)];
                case 2:
                    answer = _a.sent();
                    nextNumber = answer.nextNumber;
                    nn = nextNumber;
                    switch (nn) {
                        case 'quit':
                            keepGoing = false;
                            break;
                        case 'halt':
                            timer.halt();
                            break;
                        case 'resume':
                            timer.resume();
                            break;
                        default:
                            if (fib.isFib1000(nn)) {
                                console.log('FIB');
                            }
                            else {
                                console.log(nn);
                            }
                            listOfNumbers.increment(nn);
                            break;
                    }
                    return [3 /*break*/, 1];
                case 3: return [2 /*return*/, { timer: timer, listOfNumbers: listOfNumbers }];
            }
        });
    });
}
exports["default"] = nextNumberLoop;
;
