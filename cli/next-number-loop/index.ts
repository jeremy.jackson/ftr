import { Inquirer } from 'inquirer';

import Fibonacci from '../../lib/fibonacci';
import ListOfNumbers from '../../lib/list-of-numbers';
import Timer from '../../lib/timer';


export default async function nextNumberLoop(inquirer: Inquirer, timer: Timer, listOfNumbers: ListOfNumbers) {
  const fib = new Fibonacci();

  const validWordCommands = ['quit', 'halt', 'resume'];
  let keepGoing = true;

  const question = [
    {
      type: 'input',
      name: 'nextNumber',
      default: '0',
      message: 'Please enter the next number or one of these commands: halt, resume, or quit.',
      validate: (x: string) => {
        let logMessage = '';
        let isBigInt = false;
        let isValidWordCommand = false;

        try {
          BigInt(x);
          isBigInt = true
        } catch (err) {
          logMessage += err.message
        }

        if (validWordCommands.includes(x)) {
          isValidWordCommand = true;
        } else {
          logMessage += 'Not a valid command word.'
        }

        if (!isBigInt && !isValidWordCommand) console.log(logMessage);

        return isBigInt || isValidWordCommand;
      },
    },
  ]

  while (keepGoing) {
    const answer = await inquirer.prompt(question)
    const { nextNumber } = answer;
    const nn = <string>nextNumber;

    switch (nn) {
      case 'quit':
        keepGoing = false;
        break;
      case 'halt':
        timer.halt();
        break;
      case 'resume':
        timer.resume();
        break;
      default:
        if (fib.isFib1000(nn)) {
          console.log('FIB');
        } else {
          console.log(nn);
        }
        listOfNumbers.increment(nn);
        break;
    }
  }

  return { timer, listOfNumbers };
};
