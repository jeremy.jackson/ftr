import { Inquirer } from 'inquirer';

export default function startup(inquirer: Inquirer) {
  const question = [
    {
      type: 'number',
      name: 'outputInterval',
      message: 'Please input the number of time in seconds between emitting numbers and their frequency.',
      default: 10,
      validate: (x: any) => Number(x) > 0,
    }
  ];

  return inquirer.prompt(question);
};
